export class Machine {
  associatedNumber: string;
  clientId: string;
  label: string;
  lastServicedAt: number;
  properties: string[];
  alerts: any[];

  constructor(associatedNumber: string,
              clientId: string,
              label: string,
              properties: string[],
              lastServicedAt?: number,
              alerts?: any[]) {
    this.associatedNumber = associatedNumber;
    this.clientId = clientId;
    this.label = label;
    this.properties = properties;
    this.lastServicedAt = lastServicedAt || null;
    this.alerts = alerts || null;
  }
}
