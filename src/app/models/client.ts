export class Client {
  id: string;
  companyName: string;
  contactPerson: string;
  phone: string;
  address: string;

  constructor(companyName?, contactPerson?, phone?, address?) {
    this.companyName = companyName;
    // this.contactPerson = contactPerson;
    this.phone = phone;
    this.address = address;
  }
}
