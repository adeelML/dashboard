export class Message {
  id: string;
  sender: string;
  date: number;
  // receivedAt: number;
  message: string;

  constructor(sender, date, message) {
    this.sender = sender;
    this.date = date;
    // this.receivedAt = receivedAt;
    this.message = message;
  }
}
