export interface Alert {
  machineKey: string;
  name: string;
  condition: Condition;
}

export interface Predicate {
  property: string;
  relation: Relation;
  param: number;
}

export type Operation = 'AND' | 'OR';

export interface Expression {
  operation: Operation;
  left: Expression | Predicate;
  right: Expression | Predicate;
}

export type Condition = Expression | Predicate;

export function isPredicate(exp: Condition): exp is Predicate {
  return (<Predicate>exp).property !== undefined;
}

export enum Relation {
  EqualTo,
  NotEqualTo,
  LessThan,
  GreaterThan,
  LessThanOrEqualTo,
  GreaterThanOrEqualTo,
}

// the indices of this array correspond to the Relation
export const RELATIONS = [
  { params: 1,
    text: 'equal to',
    toStr: (prop, param) => `${prop} = ${param}` },
  { params: 1,
    text: 'not equal to',
    toStr: (prop, param) => `${prop} \u2260 ${param}` },
  { params: 1,
    text: 'less than',
    toStr: (prop, param) => `${prop} < ${param}` },
  { params: 1,
    text: 'greater than',
    toStr: (prop, param) => `${prop} > ${param}` },
  { params: 1,
    text: 'less than or equal to',
    toStr: (prop, param) => `${prop} \u2264 ${param}` },
  { params: 1,
    text: 'greater than or equal to',
    toStr: (prop, param) => `${prop} \u2265 ${param}` }
];

export function predicateToString(pred: Predicate) {
  let rel = RELATIONS[+pred.relation];
  let prop = `'${pred.property}'`;
  return rel.toStr(prop, pred.param);
}

export function conditionToString(cond: Condition) {
  let result: string;
  if (isPredicate(cond)) {
    result = predicateToString(<Predicate>cond);
  } else {
    let exp = <Expression>cond;
    result = `${conditionToString(exp.left)} ${exp.operation} ${conditionToString(exp.right)}`;
  }
  return `(${result})`;
}
