// Angular modules
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { MaterialModule } from '@angular/material';
// External modules
import { AngularFireModule } from 'angularfire2';
// Libraries
import { DatePicker } from 'ng2-datepicker/ng2-datepicker';
import { SelectModule } from 'angular2-select';
// Components
import { AlertsComponent } from './components/machine-details/alerts/alerts.component';
import { AlertFormComponent } from './components/machine-details/alerts/alert-form/alert-form.component';
import { AppComponent } from './components/app.component';
import { ClientsComponent } from './components/clients/clients.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { MachinesComponent } from './components/machines/machines.component';
import { MachineDetailsComponent } from './components/machine-details/machine-details.component';
import { MachineStatusComponent } from './components/machine-status/machine-status.component';
// Services
import { DataService } from './services/data.service';
// Pipes
import { FilterDeletedPipe } from './pipes/filter-deleted.pipe';
import { JoinPipe } from './pipes/join.pipe';
import { ExpressionFormComponent } from './components/machine-details/alerts/alert-form/expression-form/expression-form.component';
import { PredicateFormComponent } from
'./components/machine-details/alerts/alert-form/expression-form/predicate-form/predicate-form.component';
import { FilterPipe } from './pipes/filter.pipe';

// Must export the config
export const firebaseConfig = {
  apiKey: 'AIzaSyD2kJ5q_TB-H9ikqWAAkXqhJ7o8aO8i2nU',
  authDomain: 'dashboard-7dfea.firebaseapp.com',
  databaseURL: 'https://dashboard-7dfea.firebaseio.com',
  storageBucket: 'dashboard-7dfea.appspot.com',
  messagingSenderId: '864807008428'
};

@NgModule({
  declarations: [
    AlertsComponent,
    AlertFormComponent,
    AppComponent,
    ClientsComponent,
    DashboardComponent,
    MachinesComponent,
    MachineDetailsComponent,
    MachineStatusComponent,
    FilterDeletedPipe,
    JoinPipe,
    DatePicker,
    ExpressionFormComponent,
    PredicateFormComponent,
    FilterPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    SelectModule,
    AngularFireModule.initializeApp(firebaseConfig),
    MaterialModule.forRoot(),
    RouterModule.forRoot([
      {
        path: 'dashboard',
        component: DashboardComponent
      },
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'dashboard'
      },
      {
        path: 'clients',
        component: ClientsComponent
      },
      {
        path: 'machines',
        component: MachinesComponent
      },
      {
        path: 'machines/edit/:id',
        component: MachinesComponent
      },
      {
        path: 'machine/:id',
        component: MachineDetailsComponent
      }
    ]),
    SimpleNotificationsModule
  ],
  providers: [ DataService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
