import { Component } from '@angular/core';

import { DataService } from '../../services/data.service';
import { FirebaseListObservable } from 'angularfire2';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent {
  machines$: FirebaseListObservable<any[]>;
  messages$: FirebaseListObservable<any[]>;
  machineGrid: any[];

  constructor(private dataService: DataService) {
    this.machines$ = this.dataService.machines$;
    this.messages$ = this.dataService.messages$;

    this.machines$.subscribe(machinesSnapshot => {
      this.machineGrid = machinesSnapshot
                          .filter(m => !(m.hasOwnProperty('deleted') && m.deleted))
                          .map(m => m.$key);
    });
  }
}
