import { Component } from '@angular/core';

const navtabs = [
  {
    text: 'Dashboard',
    path: '/dashboard'
  },
  {
    text: 'Clients',
    path: '/clients'
  },
  {
    text: 'Machines',
    path: '/machines'
  }
];

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  navtabs;

  constructor() {
    this.navtabs = navtabs;
  }
}
