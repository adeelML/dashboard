import { Component, OnInit} from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

import { Machine } from '../../models/machine';
import { DataService } from '../../services/data.service';
import { NotificationsService } from 'angular2-notifications';
import { FirebaseListObservable } from 'angularfire2';


@Component({
  selector: 'app-machines',
  templateUrl: './machines.component.html',
  styleUrls: ['./machines.component.css']
})
export class MachinesComponent implements OnInit {
  machines$: FirebaseListObservable<any[]>;
  clients$: FirebaseListObservable<any[]>;

  selectedClientId: any;
  tempMachine: Machine;
  selectedMachineKey: string;
  editMode: boolean;
  tempLastServicedAt: string;
  tempProperties: string;

  notificationOptions = {
    position: ['bottom', 'right'],
    timeOut: 2000,
    showProgressBar: false,
    pauseOnHover: true
  };

  constructor(
    private dataService: DataService,
    private notificationService: NotificationsService,
    private route: ActivatedRoute
  ) {
    this.machines$ = this.dataService.machines$;
    this.clients$ = this.dataService.clients$;

    this.tempMachine = new Machine('', '', '', []);
    this.resetForm();
  }

  ngOnInit() {
    this.route.params.forEach((params: Params) => {
      let key = params['id'];
      if (key) {
        this.editMachine(this.dataService.getMachineByKey(key));
      }
    });
  }

  editMachine(machine): void {
    this.selectedMachineKey = machine.$key;
    this.selectedClientId = machine.clientId;

    this.tempMachine.label = machine.label;
    this.tempMachine.associatedNumber = machine.associatedNumber;

    this.tempProperties = machine.properties.join(', ');
    this.tempLastServicedAt = machine.lastServicedAt
                              ? this.makeDate(machine.lastServicedAt)
                              : machine.lastServicedAt;

    this.editMode = true;
  }

  resetForm(): void {
    this.tempMachine.label = '';
    this.tempMachine.associatedNumber = '';

    this.tempLastServicedAt = '';
    this.tempProperties = '';

    this.selectedClientId = '';
    this.editMode = false;
  }

  save(): void {
    this.tempMachine.clientId = this.selectedClientId;
    this.tempMachine.lastServicedAt = this.tempLastServicedAt
                                      ? new Date(this.tempLastServicedAt).getTime()
                                      : null;
    this.tempMachine.properties = this.parseProperties(this.tempProperties);

    if (this.editMode) {
      this.dataService.editMachine(this.selectedMachineKey, this.tempMachine);
      this.notifySuccess('Changes saved!');
    } else {
      this.dataService.addMachine(this.tempMachine);
      this.notifySuccess('Machine added!');
    }

    this.resetForm();
  }

  deleteMachine(machine): void {
    this.dataService.deleteMachine(machine.$key);
  }

  // helpers

  makeDate(timestamp) {
    let date = new Date(timestamp);
    return date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
  }

  notifySuccess(title: string, msg = ''): void {
      this.notificationService.success(title, msg);
  }

  parseProperties(propertiesInput: string): string[] {
    return propertiesInput
            .trim()
            .split(',')
            .map(p => p.trim())
            .filter(p => p.length > 0);
  }

}
