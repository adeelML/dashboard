import { Component, OnInit, Input } from '@angular/core';
import { DataService } from '../../services/data.service';
import { FirebaseListObservable } from 'angularfire2';

@Component({
  selector: 'app-machine-status',
  templateUrl: './machine-status.component.html',
  styleUrls: ['./machine-status.component.css']
})
export class MachineStatusComponent implements OnInit {
  @Input() machineKey: string;
  machine: any;
  messages$: FirebaseListObservable<any[]>;
  status: any;
  latestMsgDate: number;

  constructor(private dataService: DataService) {
    this.messages$ = this.dataService.messages$;
  }

  ngOnInit() {
    this.machine = this.dataService.getMachineByKey(this.machineKey);

    this.messages$.subscribe(machinesSnapshot => {
      let latestMsg = this.dataService.getLatestMessage(machinesSnapshot, this.machineKey);
      if (latestMsg) {
        this.latestMsgDate = latestMsg.date;
        this.status = this.dataService.parseStatus(latestMsg);
      }
    });
  }

}
