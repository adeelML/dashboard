import { Component } from '@angular/core';

import { Client } from '../../models/client';
import { DataService } from '../../services/data.service';
import { NotificationsService } from 'angular2-notifications';
import { FirebaseListObservable } from 'angularfire2';

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.css']
})
export class ClientsComponent {
  clients$: FirebaseListObservable<any[]>;
  tempClient: Client;
  selectedClientKey: string;
  editMode: boolean;

  notificationOptions = {
    position: ['bottom', 'right'],
    timeOut: 2000,
    showProgressBar: false,
    pauseOnHover: true
  };

  constructor(
    private dataService: DataService,
    private notificationService: NotificationsService
  ) {
    this.clients$ = this.dataService.clients$;
    this.tempClient = new Client();
    this.resetForm();
  }

  editClient(client): void {
    this.selectedClientKey = client.$key;
    this.tempClient.companyName = client.companyName;
    this.tempClient.address = client.address;
    this.tempClient.phone = client.phone;
    this.editMode = true;
  }

  resetForm(): void {
    this.tempClient.companyName = '';
    this.tempClient.address = '';
    this.tempClient.phone = '';
    this.editMode = false;
  }

  save(): void {
    if (this.editMode) {
      this.dataService.editClient(this.selectedClientKey, this.tempClient);
      this.notificationService.success('Changes saved!', '');
    } else {
      this.dataService.addClient(this.tempClient);
      this.notificationService.success('Client added!', '');
    }
    this.resetForm();
  }

  deleteClient(client): void {
    this.dataService.deleteClient(client.$key);
  }
}
