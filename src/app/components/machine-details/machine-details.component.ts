import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';
import { FirebaseObjectObservable, FirebaseListObservable } from 'angularfire2';

import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-machine-details',
  templateUrl: './machine-details.component.html',
  styleUrls: ['./machine-details.component.css']
})
export class MachineDetailsComponent implements OnInit {
  machine$: FirebaseObjectObservable<any>;
  messages$: FirebaseListObservable<any>;

  constructor(
    private dataService: DataService,
    private route: ActivatedRoute,
    private location: Location
  ) {
    this.messages$ = this.dataService.messages$;
  }

  ngOnInit() {
    this.route.params.forEach((params: Params) => {
      let key = params['id'];
      this.machine$ = this.dataService.getMachineObservable(key);
    });
  }

  goBack(): void {
    this.location.back();
  }

}
