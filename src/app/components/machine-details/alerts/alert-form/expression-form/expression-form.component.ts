import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';

import { Operation } from '../../../../../models/alert';

type WhichBranch = 'left' | 'right';

@Component({
  selector: 'app-expression-form',
  templateUrl: './expression-form.component.html',
  styleUrls: ['./expression-form.component.css']
})
export class ExpressionFormComponent implements OnInit {
  @Input() parentFormGroup: FormGroup;
  @Input() formBuilder: FormBuilder;
  @Input() depth: number;
  @Input() properties: string[];
  @Output() deleted: EventEmitter<any>;
  @Output() childChanged: EventEmitter<any>;


  constructor() {
    this.deleted = new EventEmitter();
    this.childChanged = new EventEmitter();
  }

  ngOnInit() {}

  isPredicate(g: FormGroup): boolean {
    return g.contains('property');
  }

  makePredicateForm(): FormGroup {
    return this.formBuilder.group({
      property: ['', Validators.required],
      relation: ['', Validators.required],
      param:    ['', Validators.required]
    });
  }

  makeExpressionForm(op: Operation, left?: FormGroup, right?: FormGroup): FormGroup {
    return this.formBuilder.group({
      operation: op,
      left: left || this.makePredicateForm(),
      right: right || this.makePredicateForm()
    });
  }

  extendSelf(op: Operation): void {
    this.childChanged.emit(this.makeExpressionForm(op, this.parentFormGroup));
  }

  updateChild(newExp: FormGroup, branch: WhichBranch): void {
    this.parentFormGroup.setControl(branch, newExp);
  }

  deleteSubExpression(branchToDelete: WhichBranch): void {
    this.parentFormGroup.setControl(branchToDelete, this.makePredicateForm());
  }

  deleteSelf() {
    if (this.depth > 0) {
      this.childChanged.emit(this.makePredicateForm());
    } else {
      this.reset();
    }
  }

  reset() {
    this.parentFormGroup = this.makePredicateForm();
  }
}
