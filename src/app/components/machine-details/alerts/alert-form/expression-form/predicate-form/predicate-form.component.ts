import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Validators, FormGroup, FormArray, FormBuilder } from '@angular/forms';

import { RELATIONS } from '../../../../../../models/alert';

@Component({
  selector: 'app-predicate-form',
  templateUrl: './predicate-form.component.html',
  styleUrls: ['./predicate-form.component.css']
})
export class PredicateFormComponent implements OnInit {
  @Input() predicateFormGroup: FormGroup;
  @Input() properties: string[];
  @Output() deleted: EventEmitter<any>;
  relations: any[];

  constructor() {
    this.relations = RELATIONS;
    this.deleted = new EventEmitter();
  }

  ngOnInit() {
  }

  deleteSelf(): void {
    this.deleted.emit(null);
  }

}
