import { Component, Input, OnInit } from '@angular/core';
import { Validators, FormGroup, FormArray, FormBuilder } from '@angular/forms';

import { DataService } from '../../../../services/data.service';
import { RELATIONS, Alert, Condition } from '../../../../models/alert';
import { NotificationsService } from 'angular2-notifications';

@Component({
  selector: 'app-alert-form',
  templateUrl: './alert-form.component.html',
  styleUrls: ['./alert-form.component.css']
})
export class AlertFormComponent implements OnInit {
  @Input() machineKey: string;
  @Input() properties: string[];
  relations: any[];


  notificationOptions = {
    position: ['bottom', 'right'],
    timeOut: 2000,
    showProgressBar: false,
    pauseOnHover: true
  };

  form: FormGroup;

  constructor(
    private dataService: DataService,
    private notificationService: NotificationsService,
    private formBuilder: FormBuilder
  ) {
    this.relations = RELATIONS;
  }

  ngOnInit(): void {
    this.initForm();
  }

  initForm(): void {
    this.form = this.formBuilder.group({
      name: ['', Validators.required],
      condition: this.makePredicateForm()
    });
  }

  makePredicateForm(): FormGroup {
    return this.formBuilder.group({
      property: ['', Validators.required],
      relation: ['', Validators.required],
      param:    ['', Validators.required]
    });
  }

  updateChild(newExp: FormGroup): void {
    this.form.setControl('condition', newExp);
  }

  save(form): void {
    let newAlert = <Alert>{
      name: form.name,
      machineKey: this.machineKey,
      condition: (<Condition>form.condition)
    };

    this.dataService.addAlert(newAlert);
    this.notifySuccess('Alert created!');
    this.resetForm();
  }

  resetForm(): void {
    this.initForm();
  }

  // helpers
  notifySuccess(title: string, msg = ''): void {
      this.notificationService.success(title, msg);
  }
}
