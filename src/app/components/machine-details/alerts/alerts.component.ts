import { Component, OnInit, Input } from '@angular/core';
import { FirebaseObjectObservable, FirebaseListObservable } from 'angularfire2';

import { DataService } from '../../../services/data.service';
import { RELATIONS, Condition, conditionToString } from '../../../models/alert';


@Component({
  selector: 'app-alerts',
  templateUrl: './alerts.component.html',
  styleUrls: ['./alerts.component.css']
})
export class AlertsComponent implements OnInit {
  @Input() machine: FirebaseObjectObservable<any>;
  alerts: FirebaseListObservable<any[]>;
  relations: any[];
  condToStr: (condition: Condition) => string;

  constructor(private dataService: DataService) {
    this.relations = RELATIONS;
    this.condToStr = conditionToString;
  }

  ngOnInit() {
    this.machine.$ref.once('value', snapshot => {
      let m = snapshot.val();
      this.alerts = this.dataService.getListObservable('machines/' + m.associatedNumber + '/alerts');
    });
  }

  delete(alertIndex: number, alertKey: string): void {
    this.dataService.deleteAlert(alertIndex, alertKey);
  }
}
