import { Injectable } from '@angular/core';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2';

import { CLIENTS, MACHINES, MESSAGES, ALERTS } from '../mock-data';
import { RELATIONS, Alert, Condition, Predicate } from '../models/alert';
import { Machine } from '../models/machine';
import { Client } from '../models/client';

@Injectable()
export class DataService {

  clients$: FirebaseListObservable<any[]>;
  machines$: FirebaseListObservable<any[]>;
  messages$: FirebaseListObservable<any[]>;
  alerts$: FirebaseListObservable<any[]>;

  constructor(private af: AngularFire) {
    this.clients$ = af.database.list('/clients');
    this.machines$ = af.database.list('/machines');
    this.messages$ = af.database.list('/messages');
    this.alerts$ = af.database.list('/alerts');
    this.populate();
  }

  populate(): void {
    this.clients$.remove();
    this.machines$.remove();
    this.messages$.remove();
    this.alerts$.remove();

    CLIENTS.forEach(c => this.addClient(c));

    // retieve firebase keys of clients
    let clientKeys: string[] = [];
    this.clients$.forEach(cs => cs.forEach(c => clientKeys.push(c.$key)));
    clientKeys = this.removeDuplicates(clientKeys);

    MACHINES.forEach((m, i) => {
      m['clientId'] = clientKeys[i % clientKeys.length];
      this.machines$.update(m.associatedNumber, m);
    }
    );

    MESSAGES.forEach(a => this.messages$.push(a));

    Object.keys(ALERTS).forEach(k => this.alerts$.update(k, ALERTS[k]));
  }

  // general methods

  join(collection1: any[], collection2Name: string, keyField: string): Promise<any[]> {
    // the collection that will be joined to collection1
    let collection2$: FirebaseListObservable<any[]>;
    // if there is a name conflict between the fields of the collections
    let disambiguatingPrefix: string;
    // if the record has been deleted, we need to do something to this field
    let fieldAffectedByDeletion: string;

    switch (collection2Name) {
      case 'machines':
        collection2$ = this.getListObservable('machines');
        disambiguatingPrefix = 'machine-';
        fieldAffectedByDeletion = 'label';
        break;
      case 'clients':
        collection2$ = this.getListObservable('clients');
        disambiguatingPrefix = 'client-';
        fieldAffectedByDeletion = 'companyName';
        break;
      case 'alerts':
        collection2$ = this.getListObservable('alerts');
        disambiguatingPrefix = 'alert-';
        fieldAffectedByDeletion = 'name';
        break;
      default:
        throw new Error('join.pipe: Unknown collection \'' + collection2Name);
    }

    let resultPromise = new Promise((resolve, reject) => {
      collection2$.$ref.once('value', snapshot => {
          let items = snapshot.val();
          let result = collection1.map(x1 => {
            let x2 = items.hasOwnProperty(x1[keyField]) ? items[x1[keyField]] : null;
            if (x2) {
              this.mergeFields(x1, x2, disambiguatingPrefix, fieldAffectedByDeletion);
            }
            return x1;
          });
          resolve(result);
        });
    });

    return resultPromise;
  }

  getListObservable(path: string): FirebaseListObservable<any[]> {
    return this.af.database.list('/' + path);
  }

  getObjectObservable(path: string): FirebaseObjectObservable<any> {
    return this.af.database.object('/' + path);
  }

  // Client methods
  addClient(newClient: Client): void {
    let newKey = this.clients$.push(null).key;
    this.clients$.update(newKey, newClient);
  }

  deleteClient(key: string): void {
    this.clients$.update(key, {deleted: true});
  }

  editClient(key: string, newInfo): void {
    this.clients$.update(key, newInfo);
  }

  getClientByKey(key: string) {
    let client;
    this.clients$.forEach(cs =>
      client = cs.find(c => c.$key === key)
    );
    return client;

  }


  // Machine methods
  addMachine(newMachine: Machine): void {
    this.machines$.update(newMachine.associatedNumber + '', newMachine);
  }

  editMachine(key: string, newInfo): void {
    this.machines$.update(key, newInfo);
  }

  deleteMachine(key: string): void {
    this.machines$.update(key, {deleted: true});
  }

  getMachineByKey(key: string): any {
    let machine;
    this.machines$.forEach(ms =>
      machine = ms.find(m => m.$key === key)
    );
    return machine;
  }

  getMachineObservable(key: string): FirebaseObjectObservable<any> {
    return this.af.database.object('machines/' + key);
  }

  // Message methods
  getLatestMessage(messages: any[], machineKey: string) {
    return messages.filter(m => m.sender === machineKey)
            .sort((a, b) => a.date - b.date)
            .pop();
  }

  parseStatus(msg) {
    let vars = msg.message.trim().split('\r\n');
    return vars.map(v => {
      let kv = v.split('=');
      return {
        key: kv[0].trim(),
        val: kv[1].trim()
      };
    });
  }

  // Alerts
  addAlert(alert: Alert): void {
    let alertKey = this.alerts$.push(alert).key;
    this.getListObservable('machines/' + alert.machineKey + '/alerts').push(alertKey);
  }

  deleteAlert(alertIndex: number, alertKey: string): void {
    this.af.database.object('alerts/' + alertKey, { preserveSnapshot: true }).take(1)
      .subscribe(snapshot => {
        let alert = snapshot.val();
        let machineKey = alert.machineKey;

        this.alerts$.remove(alertKey);
        this.machines$.remove(machineKey + '/alerts/' + alertIndex);
      });

  }


  // helpers
  private removeDuplicates(xs: any[]): any[] {
    return Array.from(new Set(xs));
  }

  private mergeFields(record1, record2, disambiguatingPrefix: string, fieldAffectedByDeletion: string) {
    let isDeleted = this.isDeleted(record2);
    Object.keys(record2).forEach(k => {
      let affectedByDeletion = (isDeleted && (k === fieldAffectedByDeletion));
      if (affectedByDeletion) {
        record1[disambiguatingPrefix + k] = record2[k] + ' [DELETED]';
      } else {
        record1[disambiguatingPrefix + k] = record2[k];
      }
    });
  }

  private isDeleted(record) {
    return (record.hasOwnProperty('deleted') && record.deleted);
  }
}
