import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(array: any[], fieldName: any, value: any): any {
    return array.filter(x => x[fieldName] === value);
  }

}
