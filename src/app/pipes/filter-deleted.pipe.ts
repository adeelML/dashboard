import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterDeleted'
})
export class FilterDeletedPipe implements PipeTransform {

  transform(xs: any): any {
    return xs ? xs.filter(x => !(x.hasOwnProperty('deleted') && x.deleted)) : xs;
  }

}
