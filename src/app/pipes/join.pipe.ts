import { Pipe, PipeTransform } from '@angular/core';
import 'rxjs/add/operator/take';

import { DataService } from '../services/data.service';

@Pipe({
  name: 'join'
})
export class JoinPipe implements PipeTransform {

  constructor(private dataService: DataService) {}

  transform(value: any, collectionName: string, keyField: string): any {
    if (!value || !collectionName || !keyField) {
      return value;
    }
    value = this.dataService.join(value, collectionName, keyField);
    return value;
  }

}
