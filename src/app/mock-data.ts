import { Client } from './models/client';

export const CLIENTS: Client[] = [
  new Client('Some Company', 'Some Name', '9048317593', 'Some place, Karachi, Pakistan'),
  new Client('Some Other Company', 'Some Other Name', '9835923523', 'Some other place, Lahore, Pakistan')
];

const properties: string[] = [
  'Pressure',
  'Temperature',
  'On Load Hours',
  'Off Load Hours',
  'Total Hours'
];

export const MACHINES = [
  {
    associatedNumber: '923362360098',
    label: 'Some machine',
    properties: properties,
    lastServicedAt: 1479896087338,
    alerts: [ '123' ]
  },
  {
    associatedNumber: '923362872482',
    label: 'Some other machine',
    properties: properties,
    lastServicedAt: 1479886087338,
    alerts: [ '456', '789' ]
  }
];

export const MESSAGES = [
    {
      'date' : new Date('Tue Oct 18 2016 09:37:39 GMT+0500 (PKT)').getTime(),
      'message' : 'Pressure = 0.00Bar\r\nTemperature = 25.61C\r\nOn Load Hours = 0\r\nOff Load Hours = 11\r\nTotal Hours = 12\r\n',
      'sender' : '923362360098'
    },
    {
      'date' : new Date('Wed Oct 19 2016 01:00:33 GMT+0500 (PKT)').getTime(),
      'message' : 'Pressure = 0.00Bar\r\nTemperature = 26.59C\r\nOn Load Hours = 0\r\nOff Load Hours = 12\r\nTotal Hours = 36\r\n',
      'sender' : '923362872482'
    }
];


export const ALERTS = {
  '123': {
    name: 'boom',
    machineKey: '923362360098',
    condition: {
      property: properties[0],
      relation: '3',
      param: 9000
    }
  },
  '456': {
    name: 'alert',
    machineKey: '923362872482',
    condition: {
      operation: 'AND',
      left: {
        property: properties[2],
        relation: '1',
        param: 1
      },
      right: {
        property: properties[1],
        relation: '1',
        param: -273
      }
    }
  },
  '789': {
    name: 'alert',
    machineKey: '923362872482',
    condition: {
      property: properties[1],
      relation: '4',
      param: -273
    }
  }
};
